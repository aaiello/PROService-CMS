var keystone = require('keystone'), 
    Types = keystone.Field.Types;
 
/**
 * Tracking Model
 * ==========
 */
 
var Tracking = new keystone.List('Tracking', { 
    defaultSort: '-createdAt',
    track: true,
});
 
Tracking.add({ 
    email: { type: String, required: false, noedit: true },
    formulario: { type: String, required: false, noedit: true },
    leido: { type: String, required: false, noedit: true },
});
 

Tracking.defaultColumns = 'createdAt, email, formulario, leido' 
Tracking.register(); 