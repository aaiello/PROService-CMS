var keystone = require('keystone'),
    Types = keystone.Field.Types;
 
var PostCategory = new keystone.List('PostCategory', {
    autokey: { path: 'slug', from: 'titulo', unique: true },
    map: { name: 'titulo' },
    defaultSort: '-createdAt',
    track: true,
});
 
PostCategory.add({
    titulo: { type: String, required: true },
    url: { type: String, default: '-', required: false, noedit: true },
});

PostCategory.relationship({ path: 'posts', ref: 'Post', refPath: 'etiquetas' });
PostCategory.defaultColumns = 'titulo'
PostCategory.register();