var keystone = require('keystone'),
    Types = keystone.Field.Types;
 
var Post = new keystone.List('Post', {
    autokey: { path: 'slug', from: 'titulo', unique: true },
    map: { name: 'titulo' },
    defaultSort: '-createdAt',
    track: true,
});
 
Post.add({
    titulo: { type: String, required: true },
    state: { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
    //author: { type: Types.Relationship, ref: 'User' },
    author: { type: String, required: false, noedit: true },
    publishedAt: Date,
    foto: {
        type: Types.LocalFile,
        dest: 'public/images',
        label: 'Foto',
        allowedTypes: [
            'image/jpeg', 'image/png'
        ],
        format: function(item, file){
            return '<img src="../../images/'+file.filename+'" style="max-width: 300px; max-height: 80px">';
        },
    },
    fotoThumb: {
        type: Types.LocalFile,
        dest: 'public/images',
        label: 'Foto Thumb',
        allowedTypes: [
            'image/jpeg', 'image/png'
        ],
        format: function(item, file){
            return '<img src="../../images/'+file.filename+'" style="max-width: 300px; max-height: 80px">';
        },
    },
    bajada: { type: Types.Html, wysiwyg: true, height: 150 },
    cuerpo: { type: Types.Html, wysiwyg: true, height: 400 },
    fuente: { type: String, default: '-', required: true },
    url: { type: String, default: '-', required: false, noedit: true },
    meta: {
        title: { type: String},
        description: { type: String}
    },
    etiquetas: { type: Types.Relationship, ref: 'PostCategory', many: true },
});
 
Post.defaultColumns = 'titulo, state|20%, author, publishedAt, foto, fotoThumb|15%'
Post.register();