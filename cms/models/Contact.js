var keystone = require('keystone'), 
    Types = keystone.Field.Types;
 
/**
 * Contact Model
 * ==========
 */
 
var Contact = new keystone.List('Contact', { 
    autokey: { from: 'nombreTaller', path: 'key', unique: true },
    defaultSort: '-createdAt',
    track: true,
});
 
Contact.add({ 
    nombre: { type: String, required: false, noedit: true },
    apellido: { type: String, required: false, noedit: true },
    dni: { type: String, required: false, noedit: true },    
    email: { type: String, required: false, noedit: true },
    sector: { type: String, required: false, noedit: true },
    cargo: { type: String, required: false, noedit: true },
    negocio: { type: String, required: false, noedit: true },
    cif: { type: String, required: false, noedit: true },
    telefonoContacto: { type: String, required: false, noedit: true },
    telefonoMovilContacto: { type: String, required: false, noedit: true },
    direccionEmpresa: { type: String, required: false, noedit: true },
    codigoPostal: { type: String, required: false, noedit: true },
    consulta: { type: String, required: false, noedit: true },
    newsletter: { type: String, required: false, noedit: true },
    formulario: { type: String, required: false, noedit: true },

    //createdAt: { type: Date, default: Date.now, noedit: true },

});
 

Contact.defaultColumns = 'createdAt, formulario, nombre, apellido, email' 
Contact.register(); 