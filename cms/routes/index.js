var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
    views: importRoutes('./views'),
};

// Setup Route Bindings
exports = module.exports = function(app) {
    app.getVersion = function(){
        return "1.3.8";
    };

    app.getObjVersion = function(){
        var obj = {
            version: app.getVersion()
        }
        return obj;        
    }

    var responseHeaders = {  
        "access-control-allow-origin": "*",
        "access-control-allow-methods": "GET, POST, PUT, DELETE, OPTIONS",
        //"access-control-allow-headers": "content-type, accept",
        //"access-control-allow-headers": "*",
        "access-control-allow-headers": "content-type, accept, X-XSRF-TOKEN",
        "access-control-max-age": 10
    };

    var responseHeadersJson = {  
        "access-control-allow-origin": "*",
        "access-control-allow-methods": "GET, POST, PUT, DELETE, OPTIONS",
        "access-control-allow-headers": "content-type, accept, X-XSRF-TOKEN",
        //"access-control-allow-headers": "*",
        "access-control-max-age": 10,
        "Content-Type": "application/json"
    };

    app.options("*",function(request, response, next){
        response.removeHeader('access-control-allow-origin');
        response.writeHead(200, responseHeadersJson);
        response.end();

    });


    // Views
    app.get('/', routes.views.index);
    //app.get('/gallery', routes.views.gallery);
    
    app.get('/api/echo/:echo', function(request, response){
        response.setHeader('access-control-allow-origin', '*');
        response.write('echo: ' + request.params.echo);  
        response.end();  

    });

    app.get('/api/version', function(request, response){
        response.setHeader('access-control-allow-origin', '*');

        var obj = app.getObjVersion();
        response.json(obj);
    });

    app.get('/api/version/:id', function(request, response){
        response.setHeader('access-control-allow-origin', '*');
        var obj = app.getObjVersion();

        if( request.params.id == "044994acfe0ffe064fb6881145057f2676001f2743c54d57ddd0867835caa8e0"){
            obj.WS_PORT = process.env.WS_PORT;
            obj.PUBLIC_PORT = process.env.PUBLIC_PORT;
            obj.PUBLIC_URL = process.env.PUBLIC_URL;
            obj.MONGO_URL = process.env.MONGO_URL;
            obj.SMTP_HOST = process.env.SMTP_HOST;
            obj.SMTP_SERVICE = process.env.SMTP_SERVICE;
            obj.SMTP_SECURE = process.env.SMTP_SECURE;
            obj.SMTP_USER = process.env.SMTP_USER;
            obj.SMTP_PASS = process.env.SMTP_PASS;
            obj.SMTP_FROM_NAME = process.env.SMTP_FROM_NAME;
            obj.SMTP_FROM_EMAIL = process.env.SMTP_FROM_EMAIL;
            obj.SMTP_TO_EMAIL = process.env.SMTP_TO_EMAIL;
        }   
        response.json(obj);
    });

    app.get('/api/gallery/list', function(request, response){
        response.setHeader('access-control-allow-origin', '*');

    
        var url = getUrl(request);

        var Slider = keystone.list('Slider');
        //console.log(Slider);

        Slider.model.find()
            .where('state', 'published')
            //.where('name', /1/i) // Buscar con una regex
            .sort([['responsive', 1], ['orderNumber', 1]])
            .exec(function(err, items) {
                //console.log(items);

                var index, len;
                for (index = 0, len = items.length; index < len; ++index) {
                    items[index]['url'] = url + items[index]['image']['path'] + '/' + items[index]['image']['filename'];
                    items[index]['url'] = items[index]['url'].replace('/public', '');
                    
                    //console.log(items[index]);
                }

                response.json(items);
            });


        //response.send('Gallery list.')
    });

    app.get('/noticias-volkswagen-proservice/:slug', function(request, response){
        response.setHeader('access-control-allow-origin', '*');

        var slug = request.params.slug;
        //slug = "primer-post";


        var Post = keystone.list('Post');
        var url = getUrl(request);
        Post.model.find()
            .where('state', 'published')
            .where('slug', slug) // Buscar con una regex
            .populate('etiquetas')
            .exec(function(err, items) {

                var title = "";
                var description = "";
                var image = "";


                if(items.length == 0){
                    title = "PRO Service Volkswagen Group";
                    description = "PRO Service es el punto de recambios originales de Volkswagen Group, un servicio que se adapta a tus necesidades. ¡De profesional a profesional!";
                    image = "http://cms.vwgproservice.es/images/c9b35d67a12d742537ee95507a60d83c.jpg";
                }else{
                    //http://cms.vwgproservice.es/noticias-volkswagen-proservice/pro-service-celebra-su-primer-aniversario-con-16-puntos-de-venta-operativos-y-mas-de-10000-clientes
                    //http://cms.vwgproservice.es/keystone/posts/59c375d71005bb100083b7ee
                    items = app.normalizarPosts(url, items);
            
                    title = items[0].meta.title;
                    description = items[0].meta.description;
                    description = description.replace(/(<([^>]+)>)/ig,"");
                    description = description.replace(/"/ig,"");
                    description = description.replace(/'/ig,"");
                    image = String(items[0].foto);
                    image = image.replace(/"/ig,"");
                    image = image.replace(/'/ig,"");
                    
                }

                var html = "";
                html = `<!doctype html>
<html>
<head>
    <title>${title}</title>
    <meta name="title" content="${title}" />
    <meta name="description" content="${description}" />    
    <meta property="og:title" content="${title}" />
    <meta property="og:description" content="${description}" />
    <meta property="og:image" content="${image}" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="1280" />
    <meta property="og:image:height" content="328" />
    <!-- Twitter Card Working Fine-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="${title}">
    <meta name="twitter:description" content="${description}">
    <meta name="twitter:image" content="${image}">    
</head>
<body>
    <h1>${title}</h1>
    <p>${description}</p>
    <img src="${image}" alt="${title}">
</body>
                </html>`;
              
                response.write(html);  
                response.end();  

            });

    });

    app.get('/api/noticias/get/:slug', function(request, response){
        response.setHeader('access-control-allow-origin', '*');

        var Post = keystone.list('Post');
        var url = getUrl(request);
        Post.model.find()
            .where('state', 'published')
            .where('slug', request.params.slug) // Buscar con una regex
            .populate('etiquetas')
            .exec(function(err, items) {
                //console.log(items);
                items = app.normalizarPosts(url, items);

                obj = {
                    errorNumber: 1,
                    errorCode: null,
                    errorMessage: null,
                    noticias: items
                }

                response.json(obj);
            });
    });


    app.get('/api/etiquetas/get/:slug', function(request, response){
        
        response.setHeader('access-control-allow-origin', '*');        
        var url = getUrl(request);

        var PostCategory = keystone.list('PostCategory');

        PostCategory.model.findOne()
            .where('slug', request.params.slug) // Buscar con una regex            
            .exec(function(err, items) {

                var categorySlug = items.slug;
                var categoryTitulo = items.titulo;                    


                var Post = keystone.list('Post');
                Post.model.find()
                    .where('state', 'published')            
                    .sort([['publishedAt', -1], ['titulo', 1]])
                    .exec(function(err, items2) {
                        //console.log(items2);
                        var index, len;
                        var listaNoticias = [];
                        var unaNoticia = null;
                        items2 = app.normalizarPosts(url, items2);
                        for (index = 0, len = items2.length; index < len; ++index) {
                            unaNoticia = items2[index];
                            if(unaNoticia.etiquetas.indexOf(items.id)>=0){
                                listaNoticias.push(unaNoticia);
                            }

                        }

                        obj = {
                            errorNumber: 1,
                            errorCode: null,
                            errorMessage: null,
                            categorySlug: categorySlug,
                            categoryTitulo: categoryTitulo,
                            noticias: listaNoticias
                        }

                        response.json(obj);
                    });                

            });


    });

    app.get('/api/noticias/latest', function(request, response){
        response.setHeader('access-control-allow-origin', '*');
        var url = getUrl(request);
        var Post = keystone.list('Post');
        //console.log(Slider);

        Post.model.find()
            .where('state', 'published')
            //.where('name', /1/i) // Buscar con una regex
            .sort([['publishedAt', -1], ['titulo', 1]])
            .populate('etiquetas')
            //.limit('3')
            .exec(function(err, items) {
                //console.log(items);
                items = app.normalizarPosts(url, items);
                items = items.slice(0, 3);
                obj = {
                    errorNumber: 1,
                    errorCode: null,
                    errorMessage: null,
                    noticias: items
                }

                response.json(obj);
            });
    });

    app.get('/api/noticias/list', function(request, response){
        response.setHeader('access-control-allow-origin', '*');
        var url = getUrl(request);
        var Post = keystone.list('Post');
        //console.log(Slider);

        Post.model.find()
            .where('state', 'published')
            //.where('name', /1/i) // Buscar con una regex
            .sort([['publishedAt', 1], ['titulo', 1]])
            .populate('etiquetas')
            .exec(function(err, items) {
                //console.log(items);
                items = app.normalizarPosts(url, items);
                obj = {
                    errorNumber: 1,
                    errorCode: null,
                    errorMessage: null,
                    noticias: items
                }

                response.json(obj);
            });
    });    

    app.normalizarPosts = function(url, items){
        var index, len;
        for (index = 0, len = items.length; index < len; ++index) {
            items[index]['slug'] = items[index].slug;

            items[index]['url'] = url + 'noticias/' + items[index].slug;
            items[index]['foto'] = url + 'images/' + items[index]['foto']['filename'];
            items[index]['fotoThumb'] = url + 'images/' + items[index]['fotoThumb']['filename'];

            var index2, len2;
            for (index2 = 0, len2 = items[index]['etiquetas'].length; index2 < len2; ++index2) {
                items[index]['etiquetas'][index2]['url'] = url + 'etiquetas/' + items[index]['etiquetas'][index2].slug;

            }
        }
        return items;
    };



    app.get('/api/puntospro/:cp', function(request, response){
        response.setHeader('access-control-allow-origin', '*');


        var url = getUrl(request);

        var obj = null;
        if( esCPValido(request.params.cp) != ""){
            obj = {
                errorNumber: 0,
                errorCode: "ERROR_CP",
                errorMessage: "CP erróneo",
            }

            response.json(obj);
            return;
        }


        var fs = require('fs');
        var unJson = fs.readFileSync('./agencias/data.json', 'utf8');
        var unaListaAgencias = JSON.parse(unJson);

        var unaAgencia = null;
        unaListaAgencias.Data.forEach(function(currentValue, index, arr){
            if(currentValue.CP.indexOf(request.params.cp) >= 0 ){
                unaAgencia = currentValue;
            }
        });

        if(unaAgencia == null){
            obj = {
                errorNumber: 0,
                errorCode: "ERROR_INEXISTENTE",
                errorMessage: "CP inexistente",
            }
        }else if(unaAgencia.Estado == false){
            obj = {
                errorNumber: 0,
                errorCode: "ERROR_PROXIMAMENTE",
                errorMessage: "Próximamente",
            }
        }else{
            var unJson = fs.readFileSync(unaAgencia.JsonFile, 'utf8');
            unJson = unJson.replace(new RegExp('/public/', 'g'), url); 
            obj = JSON.parse(unJson);           
            obj.cp = parseInt(request.params.cp);
        }

        response.json(obj);

    });

    app.createTransport = function(){
        var nodemailer = require('nodemailer');
        var transporter = null;

        if(process.env.SMTP_SERVICE != "SMTP"){
            transporter = nodemailer.createTransport({
                service: process.env.SMTP_SERVICE,

                auth: {
                    user: process.env.SMTP_USER,
                    pass: process.env.SMTP_PASS
                }
            });

        }else{
            transporter = nodemailer.createTransport({
                host: process.env.SMTP_HOST,
                port: process.env.SMTP_PORT,
                secure: process.env.SMTP_SECURE, 

                auth: {
                    user: process.env.SMTP_USER,
                    pass: process.env.SMTP_PASS
                }
            });

        }        
        return transporter;
    };

    app.enviarFormulario = function(unContacto, unFormulario){
        var obj = null;
               
        var unHtml = "<b>Nombre:</b> " + unContacto.nombre;
        unHtml += "<br><b>Apellido: </b>" + unContacto.apellido;
        unHtml += "<br><b>DNI: </b>" + unContacto.dni;
        unHtml += "<br><b>Email: </b>" + unContacto.email;        
        unHtml += "<br><b>Sector: </b>" + unContacto.sector;
        unHtml += "<br><b>Cargo: </b>" + unContacto.cargo;
        unHtml += "<br><b>Negocio: </b>" + unContacto.negocio;
        unHtml += "<br><b>CIF: </b>" + unContacto.cif;
        unHtml += "<br><b>Telefono Contacto: </b>" + unContacto.telefonoContacto;
        unHtml += "<br><b>Telefono Móvil Contacto: </b>" + unContacto.telefonoMovilContacto;        
        unHtml += "<br><b>Direccion Empresa: </b>" + unContacto.direccionEmpresa;
        unHtml += "<br><b>Codigo Postal: </b>" + unContacto.codigoPostal;
        unHtml += "<br><b>Consulta: </b>" + unContacto.consulta;
        unHtml += "<br><b>Newsletter: </b>" + unContacto.newsletter;

        unHtml = unHtml.replace(/undefined/g, "No se ha ingresado");
        

        var unTexto = unHtml.replace(/<b>/g, "");
        unTexto = unTexto.replace(/<\/b>/g, "");
        unTexto = unTexto.replace(/<br>/g, "");

        
        keystone.createItems({
            
            Contact: [{
                nombre: unContacto.nombre,
                apellido: unContacto.apellido,
                dni: unContacto.dni,
                email: unContacto.email,
                sector: unContacto.sector,
                cargo: unContacto.cargo,
                negocio: unContacto.negocio,
                cif: unContacto.cif,
                telefonoContacto: unContacto.telefonoContacto,
                telefonoMovilContacto: unContacto.telefonoMovilContacto,  
                direccionEmpresa: unContacto.direccionEmpresa,
                codigoPostal: unContacto.codigoPostal,
                consulta: unContacto.consulta,
                newsletter: unContacto.newsletter,
                formulario: unFormulario.id
            }]
        }, function(err, stats) {
            stats && console.log(stats.message);
            //done(err);
        });

        app.enviarMailFormulario(process.env.SMTP_TO_EMAIL, process.env.SMTP_CC_EMAIL, unFormulario.subject, 'Mensaje: ' + unTexto, '<b>Mensaje:</b><br>' + unHtml);

        if(unFormulario.tpl != null){
            app.enviarMailFormulario(unContacto.email, '', unFormulario.subject, '.', unFormulario.tpl);
        }

        if(obj == null) {
            obj = {
                errorNumber: 1,
                errorCode: "",
                errorMessage: "",
            }
        }

        return obj;
    };


    app.enviarMailFormulario = function(varTo, varBcc, varSubject, varText, varHtml){
        try {

            var transporter = this.createTransport();

            // setup email data with unicode symbols
            var mailOptions = {
                from: '"' + process.env.SMTP_FROM_NAME + '" <' + process.env.SMTP_FROM_EMAIL + '>', // sender address
                to: varTo, // list of receivers
                bcc: varBcc, // list of receivers
                subject: varSubject, // Subject line
                text: varText, // plain text body
                html: varHtml // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);

            });
        }
        catch(err) {
            console.log(err.message);
        }


    };

    app.get('/api/signature/:id', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var Tracking = keystone.list('Tracking');

        Tracking.model.findById()
            .where('_id', request.params.id) // Buscar con una regex
            .exec(function(err, items) {
                var obj = {}
                if(!items){
                    obj = {
                    }
                    response.json(obj);
                }else{
                    items.leido = "true";
                    items.save();
                    obj = {
                    }
                    response.json(obj);

                }
            });


    });

    app.post('/api/contact', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;

        var Tracking = keystone.list('Tracking');         
        var unObjeto = new Tracking.model({
            email: unContacto.email,
            formulario: "CONTACTO",
            leido: "false"
        });
        unObjeto.save(function(err) {
        });

        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/confirma-contacto.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKID}}', 'g'), '' + unObjeto._id + '');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKING}}', 'g'), '<img src="__URLBASE__/api/signature/' + unObjeto._id + '" style="display:none;" border=0 width=1 height=1 alt="">');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"

        
        obj = app.enviarFormulario(unContacto, {id: "CONTACTO", subject: "Contacto PRO Service", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/formulario-visitantes', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;

        var Tracking = keystone.list('Tracking');
        var unObjeto = new Tracking.model({
            email: unContacto.email,
            formulario: "NEWS LETTER",
            leido: "false"
        });
        unObjeto.save(function(err) {
        });

        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/confirma-registro.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKID}}', 'g'), '' + unObjeto._id + '');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKING}}', 'g'), '<img src="__URLBASE__/api/signature/' + unObjeto._id + '" style="display:none;" border=0 width=1 height=1 alt="">');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "NEWS LETTER", subject: "Bienvenido a PRO Service", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/formulario-soy-mecanico', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/soy-mecanico-tpl.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "SOY MECANICO", subject: "Bienvenido a PRO Service", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/open-pro-girona', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/open-pro-tpl.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "OPENPRO-GIRONA", subject: "Confirmación de asistencia OPEN PRO", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/open-pro-barcelonaoest', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/open-pro-tpl.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "OPENPRO-BARCELONA-OEST", subject: "Confirmación de asistencia OPEN PRO", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/open-pro-tarragona', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/open-pro-tpl.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "OPENPRO-TARRAGONA", subject: "Confirmación de asistencia OPEN PRO", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/formulario-meetingpoint', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/meetingpoint-tpl.html', 'utf8');
        var url = getUrl(request);
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        obj = app.enviarFormulario(unContacto, {id: "MEETING-POINT", subject: "Confirmación de asistencia PRO SERVICE MEETING POINT", tpl: unHtmlTpl});
        response.json( obj );
    });

    app.post('/api/baja-newsletter', function(request, response){
        response.setHeader('access-control-allow-origin', '*');

        var obj = null;

        var unEmail = request.body.email;
        if(unEmail == null){
            obj = {
                errorNumber: 0,
                errorCode: "PARAMETRO_INVALIDO",
                errorMessage: "No se ha recibido email",
            }

            response.json( obj );
            return;
        }

        unEmail = unEmail.replace(new RegExp('%40', 'g'), '@');

        var Contact = keystone.list('Contact');

        Contact.model.find()
            .where('email', unEmail) // Buscar con una regex
            .exec(function(err, items) {
                var obj = null;
                if(items.length == 0){
                    obj = {
                        errorNumber: 0,
                        errorCode: "PERSONA_NO_ENCONTRADA",
                        errorMessage: "No se ha encontrado la persona buscada"
                    }
                }else{
                    items.forEach(function(entry) {
                        entry.newsletter = "false";
                        entry.save();

                    });
                    app.enviarMailBajaNewsletter(unEmail);
                }
                
                if(obj == null) {
                    obj = {
                        errorNumber: 1,
                        errorCode: "",
                        errorMessage: "",
                    }
                }

                response.json( obj );

            });
    });


    app.enviarMailBajaNewsletter = function(unEmail){
        var unHtml = "<b>Email: </b>" + unEmail;
       
        var unTexto = unHtml.replace(/<b>/g, "");
        unTexto = unTexto.replace(/<\/b>/g, "");
        unTexto = unTexto.replace(/<br>/g, "");

        try {            
            var transporter = this.createTransport();

            // setup email data with unicode symbols
            var mailOptions = {
                from: '"' + process.env.SMTP_FROM_NAME + '" <' + process.env.SMTP_FROM_EMAIL + '>', // sender address
                //to: process.env.SMTP_TO_EMAIL, // list of receivers
                to: 'bajas@vwgproservice.es', // list of receivers
                bcc: process.env.SMTP_CC_EMAIL, // list of receivers
                subject: 'Baja newsletter', // Subject line
                text: 'Mensaje: ' + unTexto, // plain text body
                html: '<b>Mensaje:</b><br>' + unHtml // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    obj = {
                        errorNumber: 0,
                        errorCode: "ERROR_MAIL",
                        errorMessage: "No se ha podido enviar el mensaje",
                    }

                    console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);

            });
        }
        catch(err) {
            obj = {
                errorNumber: 0,
                errorCode: "ERROR_MAIL",
                errorMessage: "No se ha podido enviar el mensaje. " + err.message,
            }

            console.log(err.message);
        }

    };

    app.get('/api/puntospro3/:cp', function(request, response){
        response.writeHead(200, responseHeaders);

        var url = getUrl(request);

        var obj = null;
        if( esCPValido(request.params.cp) != ""){
            obj = {
                errorCode: 0,
                errorMessage: "CP erróneo",
            }

            response.json(obj);
            return;
        }

        var unaAgencia = null;

        var Agencia = keystone.list('Agencias');

        Agencia.model.find()
            .where('cp', new RegExp('/' + cpStr + '/i')) // Buscar con una regex
            .exec(function(err, items) {

                var index, len;
                for (index = 0, len = items.length; index < len; ++index) {
                    //items[index]['url'] = items[index]['url'].replace('/public', '');
                    // unJson = unJson.replace(new RegExp('/public/', 'g'), url);  ver como hacer esto...
                    items[index]['cp'] = cpInt;
                }

                unaAgencia = items[0];
            });

        if(unaAgencia == null){
            obj = {
                errorCode: 0,
                errorMessage: "CP inexistente",
            }
        }else if(unaAgencia.Estado == false){
            obj = {
                errorCode: 0,
                errorMessage: "Próximamente",
            }
        }else{
            var unJson = fs.readFileSync(unaAgencia.JsonFile, 'utf8');
            unJson = unJson.replace(new RegExp('/public/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
            obj = JSON.parse(unJson);           
            obj.cp = parseInt(request.params.cp);
        }

        response.json(obj);
    });

    app.get('/api/template/:id', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/' + request.params.id + '.html', 'utf8');
        var url = getUrl(request);
        
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        
        response.write( unHtmlTpl );
        response.end();
    });

    app.get('/api/template/:id/:trackid', function(request, response){
        var obj = null;
        response.setHeader('access-control-allow-origin', '*');

        var unContacto = request.body;
        var fs = require('fs');
        var unHtmlTpl = fs.readFileSync('./templates/' + request.params.id + '.html', 'utf8');
        var url = getUrl(request);

        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKID}}', 'g'), '' + request.params.trackid + '');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('{{TRACKING}}', 'g'), '<img src="__URLBASE__/api/signature/' + request.params.trackid + '" style="display:none;" border=0 width=1 height=1 alt="">');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('http://proservicecms.desa.aeoninfinity.com.ar', 'g'), '__URLBASE__');
        unHtmlTpl = unHtmlTpl.replace(new RegExp('__URLBASE__/', 'g'), url);  // Traer esto tambien de la base, en el mismo "data"
        
        response.write( unHtmlTpl );
        response.end();
    });


    
}


function esCPValido(unCp){
    unCp = "" + unCp;
    var patt1 = /[0-9]{5}/g; 

    if(! unCp.match(patt1)){
        return "No tiene 5 caracteres";
    }   

    if(unCp.match(patt1)[0] != unCp){
        return "No tiene 5 caracteres";
    }   

    if( parseInt(unCp.substr(0, 2)) == 0 ){
        return "No puede comenzar con 00";
    }

    if( parseInt(unCp.substr(0, 2)) > 52 ){
        return "No puede comenzar con código mayor a 52";
    }

    if( parseInt(unCp.substr(2, 3)) == 0 ){
        return "No puede finalizar con 000";
    }

    if(parseInt(unCp) > 52006){
        return "No puede ser superior a 52006";
    }

    return "";

}

function getUrl(request){
    var port = process.env.PUBLIC_PORT || request.socket.localPort;
    var host = process.env.PUBLIC_URL || request.hostname;
    if(parseInt(port)===80){
        port = "";
    }else{
        port = ':' + port;
    }
    var url = request.protocol  + '://' + host + port + '/';

    return url;
}