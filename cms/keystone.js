// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Require keystone
var keystone = require('keystone');
var Twig = require('twig');

keystone.init({
	'name': 'Pro Service CMS',
	'brand': 'Pro Service CMS',

	'less': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'twig',

	'twig options': { method: 'fs' },
	'custom engine': Twig.render,

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'signin logo': '/images/logo.png',
	
	'port': process.env.WS_PORT,
	'mongo': process.env.MONGO_URL
});
keystone.import('models');
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});
keystone.set('routes', require('./routes'));
keystone.set('nav', {
	sliders: 'sliders',
	//galleries: 'galleries',
	users: 'users',

});

keystone.start();
