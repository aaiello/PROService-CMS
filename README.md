# Project Name

PRO Service - CMS

## Installation

1) Configurar el acceso a mongo en el archivo .env <br />
	MONGO_URL=mongodb://USER:PASS@IP:PORT/DATABASE <br />
	<br />	
2) Configurar el puerto que se escuchará en el archivo .env	<br />
	WS_PORT=8080 <br />
	<br />
3) Configurar el correo en el archivo .env	<br />
	Servidor SMTP: SMTP_HOST=smtp.gmail.com<br />
	En caso de ser gmail, sino "otro": SMTP_SERVICE=gmail<br />
	SMTP_SECURE=false <br />
	SMTP_USER=xxx@xxx.com <br />
	SMTP_PASS=xxx <br />
	Nombre que se mostrará en el from: SMTP_FROM_NAME=Ismael Luque <br />
	Email que se usará en el from: SMTP_FROM_EMAIL=aaiello@aeon.com.ar <br />
	Cuenta a donde se enviarán los mensajes: SMTP_TO_EMAIL=aaiello@aeon.com.ar <br />
	<br />
4) Configurar el puerto que se escuchará en el dockerfile	<br />
	EXPOSE 8080	<br />
	<br />
5) Usar dockerfile	<br />
	docker build -t proservicecmsimage .	<br />
	<br />
6) Configurar smtp de email para envio de correos en el archivo .env	<br />
	Ver configuración en: https://nodemailer.com/about/	<br />
	<br />
## Usage

docker run --name proservicecmsinstance -p 49160:8080 -d proservicecmsimage

## Contributing

TODO: Write contributing

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license